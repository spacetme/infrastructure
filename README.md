
bmsurge
=======

The infrastructure that runs [Be-Music Surge](http://be-music.surge.sh).
Using microservice approach.


- `nowplaying` Serves the server status and currently playing music.
- `programme` Scripts to update radio program.
- `statistics-logger` Worker that records the server statistics every minute.
- `website` The web site.
